from django.db import models

class Project(models.Model):
	title = models.CharField(max_length=50)
	description = models.TextField()
	link = models.URLField()
	link_case_study = models.URLField(null=True, blank=True)
	thumb = models.ImageField(upload_to="uploads/thumbs")

	def __unicode__(self):
		return self.title