from django.views.generic import View
from django.shortcuts import render
from .models import Project

class IndexView(View):
    def get(self, request):
        context = { 
        	'projects': Project.objects.all()
        }
        return render(self.request, 'index.html', context)

    def post(self, request, *args, **kwargs):
        pass
