define(function(require) {

    var $ = require('jquery');

    $('a[rel=internal]').on('click', function(e) {
        e.preventDefault();
        var anchor = $(this).attr('rel').split('#')[1];
        var section = $('#' + anchor);
        $('html,body').animate({
            scrollTop: section.position.top
        });
    });

});